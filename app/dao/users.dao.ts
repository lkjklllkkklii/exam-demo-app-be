import UserDomainInterface from "../interfaces/user.domain.interface";

export default class UserDao {

    constructor(
        private _userModel: any,
    ) { }

    /**
    * Find user by email
    * @param {String} email The email of user
    * @returns {Object} The table row of user
    */
    
    async findOneByUsernameAsync(email:string) {
        return await this._userModel.findOne({where: { email }});
    }

    /**
    * Find user by email and password
    * @param {String} email The email of user
    * @param {String} password The hashed password of user
    * @returns {Object} The table row of user
    */

    async findOneByEmailAndPasswordAsync(email:string, password: string) {
        return await this._userModel.findOne({where: { email, password }});
    }

    /**
     * Create user
     * @param {Object} {} a row of user
     * @returns {Object} The generated row with id
     */

    async createAsync({email, password, firstName, lastName}: UserDomainInterface):Promise<UserDomainInterface> {
        const user = await new this._userModel({ email, password, firstName, lastName });
        await user.save();
        return user;
    }

    /**
     * 
     * @param {Number} id The id of user
     * @returns {Object} The table row of user
     */
    async findByIdAsync(id:number) {
        return await this._userModel.findOne({where: {id}})
    }

    /**
     * 
     * @returns {Object[]} list of users
     */
    async findAllAsync() {
        return await this._userModel.findAll();
    }
}
