require("dotenv").config();

export default class Constants {
    static get ACCESS_TOKEN_SECRET () {
        return process.env.ACCESS_TOKEN_SECRET;
    }
    static get SALT() {
        return process.env.SALT;
    }
    static get NODE_ENV() {
        return process.env.NODE_ENV;
    }
    static get EXAM_DEMO_APP_PORT() {
        return process.env.EXAM_DEMO_APP_PORT || 5000;
    }
    static get EXAM_DEMO_APP_DB_NAME() {
        return process.env.EXAM_DEMO_APP_DB_NAME;
    }
    static get EXAM_DEMO_APP_DB_USERNAME() {
        return process.env.EXAM_DEMO_APP_DB_USERNAME;
    }
    static get EXAM_DEMO_APP_DB_PASSWORD() {
        return process.env.EXAM_DEMO_APP_DB_PASSWORD;
    }
    static get EXAM_DEMO_APP_DB_DIALECT() {
        return process.env.EXAM_DEMO_APP_DB_DIALECT;
    }
}