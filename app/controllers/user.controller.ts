import { Request, Response } from "express";
import UserDomainInterface from "../interfaces/user.domain.interface";
import UserServiceInterface from "../interfaces/user.service.interface";
import AuthValidation from "../validations/auth.validation";

export default class UserController {
    constructor(
        private _userService: UserServiceInterface,
        private _authValidation: AuthValidation
    ) {}

    async loginAsync(req:Request, res:Response) {
        try {
            const { email, password } = req.body;                 
            const user = await this._userService.login(email, password);
            const accessToken = await this._authValidation.generateToken(user.id);
            res.json({ status: 'success', user, accessToken });
        } catch (error: any) {            
            if(error.message == "Invalid login") res.send(401);
            else res.sendStatus(500);
        }
    }

    async registerAsync(req:Request, res:Response) {
        try {
            const { email, password, firstName, lastName }: UserDomainInterface = req.body;
            const user = await this._userService.register({email, password, firstName, lastName});
            delete user.password;
            res.json({  status: 'success', user});
        } catch (error) {
            res.sendStatus(500);
        }
    }

    async findOneByIdAsync(req: Request, res: Response) {
        try {
            const {id} = req.params;
            const authHeader = req.headers['authorization'];
            const token = authHeader && authHeader.split(' ')[1];
            if(token == null) return res.sendStatus(401);

            const isValid = await this._authValidation.authenticateToken(token);
            if(!isValid) return res.sendStatus(403);
            const user = await this._userService.findById(id);
            res.json({  status: 'success', user});
        } catch (error) {
            console.log(error);
            
            res.sendStatus(404);
        }
    }

    async findAllAsync(req: Request, res: Response) {
        try {
            const authHeader = req.headers['authorization'];
            const token = authHeader && authHeader.split(' ')[1];
            if(token == null) return res.sendStatus(401);
            const isValid = await this._authValidation.authenticateToken(token);
            if(!isValid) return res.sendStatus(403);
            const data = await this._userService.findAll();
            res.json({  status: 'success', data });
        } catch (error) {
            res.sendStatus(404);
        }
    }
}
