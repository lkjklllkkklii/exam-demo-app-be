import Sequelize, { DataTypes } from "sequelize";
const db = require("../utils/database.utils");

const schema = db.define("users", {
    id: { type: Sequelize.BIGINT, primaryKey: true, autoIncrement: true },
    email: { type: Sequelize.STRING, allowNull: false },
    password: { type: Sequelize.STRING, allowNull: false },
    firstName: { type: Sequelize.STRING, allowNull: false },
    lastName: { type: Sequelize.STRING, allowNull: false },
  },
  { defaultScope: { attributes: { exclude: ['password'] } } },
  { indexes: [ {unique: true, fields: ["email"] } ] },  
);

module.exports = schema;
