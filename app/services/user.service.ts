import Constants from "../others/constants";
import UserDomainInterface from "../interfaces/user.domain.interface";
import UserServiceInterface from "../interfaces/user.service.interface";

export default class UserService implements UserServiceInterface {
    constructor(
        private _userDao: any,
        private _bcrypt: any,
        private _jwt: any
    ) {}

    async login(email: string, password:string) {
        let {_userDao, _bcrypt, _jwt} = this;
        const hashedPassword = await _bcrypt.hash(password, Constants.SALT);
        let user: UserDomainInterface = await _userDao.findOneByEmailAndPasswordAsync(email, hashedPassword);
        if(user == null) throw new Error("Invalid login");
        return user;
    }
    async register({ email, password, firstName, lastName }: UserDomainInterface) {
        let {_userDao, _bcrypt} = this;
        const hashedPassword = await _bcrypt.hash(password, Constants.SALT);
        return await _userDao.createAsync({ email, password: hashedPassword, firstName, lastName });
    }
    async findById(id:string) {
        let {_userDao} = this;
        return await _userDao.findById(id);        
    }
    async findAll() {
        return await this._userDao.findAll();
    }

}