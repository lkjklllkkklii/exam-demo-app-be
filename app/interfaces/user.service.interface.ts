import UserDomainInterface from "./user.domain.interface";

export default interface UserServiceInterface {
    login(username: string, password:string):Promise<any>,
    register(userDomain: UserDomainInterface): Promise<any>,
    findById(id:string): Promise<any>
    findAll(): Promise<any>;
}


