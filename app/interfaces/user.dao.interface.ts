import UserDomainInterface from "./user.domain.interface";

export default interface UserDaoInterface {
    findOneByUsernameAsync(username:string): Promise<UserDomainInterface>,
    findOneByEmailAndPasswordAsync(username:string, password: string): Promise<UserDomainInterface>,
    createAsync({email, password, firstName, lastName}: UserDomainInterface):Promise<UserDomainInterface>,
}