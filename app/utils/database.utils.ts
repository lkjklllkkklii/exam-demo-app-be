import Constants from "../others/constants";

const Sequelize = require("sequelize");

module.exports = new Sequelize(Constants.EXAM_DEMO_APP_DB_NAME, Constants.EXAM_DEMO_APP_DB_USERNAME, Constants.EXAM_DEMO_APP_DB_PASSWORD, {
  dialect: "mssql",
  operatorAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});