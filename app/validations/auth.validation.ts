import Constants from "../others/constants";


export default class AuthValidation {

    constructor(private _jwt: any){}
    
    authenticateToken(token:string) {
        let {_jwt} = this;
        return new Promise((resolve,reject) => {
            _jwt.verify(token, Constants.ACCESS_TOKEN_SECRET, (err:any) => {
                if(err) return resolve(false);
                return resolve(true);
            });
        });
        
    }
    generateToken(id:number) {
        let {_jwt} = this;
        return _jwt.sign({ id }, Constants.ACCESS_TOKEN_SECRET);
    }
}