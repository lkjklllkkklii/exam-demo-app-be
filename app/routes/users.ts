import express from "express";
const {userController} = require('../../singleton');
const router = express.Router();

router.post("/login", (req,res) => userController.loginAsync(req,res));
router.post("/register", (req,res) => userController.registerAsync(req,res));
router.get("/:id", (req,res) => userController.findOneByIdAsync(req,res));
router.get("/", (req,res) => userController.findAllAsync(req,res));

module.exports = router;