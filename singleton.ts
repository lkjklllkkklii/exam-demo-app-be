import UserController from "./app/controllers/user.controller";
import UserDao from "./app/dao/users.dao";
import UserDaoInterface from "./app/interfaces/user.dao.interface";
import UserServiceInterface from "./app/interfaces/user.service.interface";
import UserService from "./app/services/user.service";
import AuthValidation from "./app/validations/auth.validation";

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

let authValidation = new AuthValidation(jwt);
let userModel = require("./app/models/Users");
let userDao: UserDaoInterface = new UserDao(userModel);
let userService: UserServiceInterface = new UserService(userDao, bcrypt, jwt);
let userController = new UserController(userService, authValidation);

module.exports = {userController, authValidation};