import Constants from "./app/others/constants";

const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const fs = require("fs");
const path = require("path");
const jwt = require("jsonwebtoken");

/**
 * MIDDLEWARES
 */
app.use(cors({ origin: true, credentials: true }));
app.use(bodyParser.json());

/**
 * Reading files inside app/routes and generating url
 */
const apiPath: string = "/app/routes";
const apiRoute: string = "/api";
fs.readdirSync(path.join(__dirname, "app/routes/")).forEach((file: string) => {
  if (["index.ts", "api-db-static", "requests", "request"].includes(file)) return;
  const fileName = path.parse(file).name;
  app.use(`${apiRoute}/${fileName}`, require(`.${apiPath}/${fileName}`));
  console.log(path.parse(file).name + " loaded");
});

const sequelize = require("./app/utils/database.utils");
sequelize.authenticate().then(() => console.log("Database connect...")).catch((err: any) => console.log(err));

const port = Constants.EXAM_DEMO_APP_PORT;
app.listen(port, async () => {
  console.log(`Server running on port ${port}...`);
  await sequelize.sync({ alter: true });
  console.log(`Database synced!`);
});